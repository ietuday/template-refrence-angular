import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabContainerTestComponent } from './tab-container-test.component';

describe('TabContainerTestComponent', () => {
  let component: TabContainerTestComponent;
  let fixture: ComponentFixture<TabContainerTestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabContainerTestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabContainerTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
