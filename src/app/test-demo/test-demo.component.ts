import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-demo',
  templateUrl: './test-demo.component.html',
  styleUrls: ['./test-demo.component.css']
})
export class TestDemoComponent implements OnInit {
  totalEstimate = 10;
  ctx = {estimate: this.totalEstimate};

  constructor() { }

  ngOnInit() {
  }

}
