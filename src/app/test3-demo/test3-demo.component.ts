import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test3-demo',
  templateUrl: './test3-demo.component.html',
  styleUrls: ['./test3-demo.component.css']
})
export class Test3DemoComponent implements OnInit {
  loginText = 'Login';
  signUpText = 'Sign Up';
  lessons = ['Lesson 1', 'Lessons 2'];
  constructor() { }

  ngOnInit() {
  }

  login() {
    console.log('Login');
  }

  signUp() {
    console.log('Sign Up');
  }

}
