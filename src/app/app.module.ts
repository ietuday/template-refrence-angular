import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {TestEsComponent} from './test-es/test-es.component';
import {ElasticsearchService} from './elasticsearch.service';
import { TabContainerComponent } from './tab-container/tab-container.component';
import 'rxjs/add/observable/of';
import { TestDemoComponent } from './test-demo/test-demo.component';
import { Test2DemoComponent } from './test2-demo/test2-demo.component';
import { TabContainerTestComponent } from './tab-container-test/tab-container-test.component';
import { Test3DemoComponent } from './test3-demo/test3-demo.component';
import { CounterComponent } from './counter/counter.component';
import { WrapperComponent } from './wrapper/wrapper.component';

@NgModule({
  declarations: [
    AppComponent,
    TestEsComponent,
    TabContainerComponent,
    TestDemoComponent,
    Test2DemoComponent,
    TabContainerTestComponent,
    Test3DemoComponent,
    CounterComponent,
    WrapperComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [ElasticsearchService],
  bootstrap: [AppComponent]
})

export class AppModule {}
