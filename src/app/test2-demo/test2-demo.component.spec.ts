import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Test2DemoComponent } from './test2-demo.component';

describe('Test2DemoComponent', () => {
  let component: Test2DemoComponent;
  let fixture: ComponentFixture<Test2DemoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Test2DemoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Test2DemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
