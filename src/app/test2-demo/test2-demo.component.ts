import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'app-test2-demo',
  templateUrl: './test2-demo.component.html',
  styleUrls: ['./test2-demo.component.css']
})
export class Test2DemoComponent implements OnInit {
  @ViewChild('defaultTabButtons')
  private defaultTabButtonsTpl: TemplateRef<any>;

  constructor() { }

  ngOnInit() {
    console.log("defaultTabButtonsTpl",this.defaultTabButtonsTpl);
    
  }

}
