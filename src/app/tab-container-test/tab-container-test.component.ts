import { Component, OnInit, TemplateRef, Input } from '@angular/core';

@Component({
  selector: 'app-tab-container-test',
  templateUrl: './tab-container-test.component.html',
  styleUrls: ['./tab-container-test.component.css']
})
export class TabContainerTestComponent implements OnInit {
  @Input()
  headerTemplate: TemplateRef<any>;
  constructor() { }

  ngOnInit() {
  }

}
