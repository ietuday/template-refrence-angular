import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Observable } from "rxjs/Observable";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  loginText = 'Login';
  signUpText = 'Sign Up';
  course = {
    id:1,
    description: 'Angular For Beginners'
};
  lessons = ['Lesson 1', 'Lessons 2'];

  totalEstimate = 10;

  templateCtx = { estimate: this.totalEstimate };


  @ViewChild('defaultTabButtons')
  private defaultTabButtonsTpl: TemplateRef<any>;


  ngOnInit() {
    console.log(this.defaultTabButtonsTpl);
  }

  login() {
    console.log('Login');
  }

  signUp() {
    console.log('Sign Up');
  }
}
